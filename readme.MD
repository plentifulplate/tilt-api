
# Tilt Pi - Read Only Viewer

The purpose of this project is to present a read-only view of the data being returned from the [tilt hydrometer](http://www.tilthydrometer.com). There are multiple ways of viewing this data including dedicated mobile apps, a nifty [google sheets](https://tilthydrometer.com/blogs/news/easiest-way-yet-to-post-your-tilt-to-google-sheets) integration, as well a [tilt pi](https://tilthydrometer.com/products/tilt-pi-raspberry-pi-disk-image-download) distribution for Raspberry Pi. Whilst these are definitely handy tools, I found I wanted to utilize the grid-style panel view being rendered by the tilt pi, but not allowing the world to edit my configuration or redirect my data somewhere I don't want it. Would people really do this you ask? Probably not. But this seemed like a good idea at the time so here we are!

![Tilt Pi API - Demo](/public/images/tiltpi-api-demo.png)

## Caveats
I am NOT a node.js/npm/express.js developer. I am a developer by trade, but this was a fun little experiment trying to get my head around these front end technologies. Use at your own risk. I offer no guarantees about the quality, suitability not security of this code. Again, ***USE AT YOUR OWN RISK!***

  
## Requirements

Obviously you will need a tilt or tilt pro hydrometer. You will also need an internet-connected device close enough to the tilt hydrometer to be able to take a reading. Typically this will be either a mobile device (iPad/iPhone/Android/etc) that is "always awake" and running the tilt app for that platform. An alternative (and the solution I settled on) is to use the [tilt pi](https://tilthydrometer.com/products/tilt-pi-raspberry-pi-disk-image-download) Raspberry Pi. Setting this up is quite simple and well documented on the tilt hydrometer site.
You will also need to have [npm](https://www.npmjs.com/) and [Node.js](https://nodejs.org/) installed.
 

## Running the application

 
First, you need to grab a copy of the source code from [bitbucket](https://bitbucket.org/plentifulplate/tilt-api/src/master/).
In a console, change to a directory of your choice, e.g. `C:\work\tiltpi`
Then, clone the repository `git clone git@bitbucket.org:plentifulplate/tilt-api.git`
Once that has completed, you need to install it: `npm install`
Then to actually run the service: `npm start`

You should see console output that looks similar to this (depending on your location):
  

> tiltpi@0.0.0 start C:\work\tiltpi  
> node ./bin/www

In theory, you should be now able to view the demo of tiltpi by going to [http://localhost:3000/tilt](http://localhost:3000/tilt)  
If you are running this on a different computer then obviously swap out localhost for the machines IP/hostname.  
When the app has not received any data, it defaults to demo mode. You can always view the demo mode by appending the `?demo=true` querystring to the url: [http://localhost:3000/tilt?demo=true](http://localhost:3000/tilt?demo=true)  
Same caveats apply for IP/hostname obviously.

  

## Sending data to the app

It is a fairly straightforward process using the tilt app or the tilt pi interface. You just need to add the base URL [http://localhost:3000/](http://localhost:3000/) (or whatever the correct IP/hostname is)

For the mobile app:  
`Cloud Settings > Enter Logging URL`

For Tilt Pi:  
First, tap the hamburger menu in the top left, then `Logging > Cloud Settings > Cloud URL`  
But wait, I hear you say! I already have a cloud URL configured! That's okay. All you need to do is comma separate the list. So, if you already have a URL in there, lets say a google sheets url like this:  
https://script.google.com/macros/s/xxxxxxxxxxxxxxxxxx/exec  
All you need to do is append a comma, then the URL for your app:  
https://script.google.com/macros/s/xxxxxxxxxxxxxxxxxx/exec,http://localhost:3000  
This way you still log to your existing cloud provider as well as the tilt read-only front end.

 
there are 2 query string parameters you can use:  
demo=* - if this query string parameter is set with *ANY* value, it will display the demo data.  
celsius=true|flase - toggle between celsius and farrenheit (default is celsius)

  
## Accuracy of data displayed

I am sure there is lots to pick apart with this work. I basically completely made up the gravity percentage/attenuation reading - I have zero idea if this is even remotely correct. If someone has a better solution for this calculation I am all ears! Similar to the temperature display. Made up. Sorry.

  

## License

You are free to use this for personal or educational use.

  
## Support

In keeping with the no warranty, no license theme - support is whatever you can find on google. Sorry, but I am far from proficient at node.js nor the tilt hydrometer other than my personal use.

If you want to let me know you are using it feel free to drop me an email at devmattredman [at] gmail [dot] com - but any requests for support will be ignored.