var express = require('express');
var numeral = require('numeral');
var moment = require('moment');

var fs = require('fs');
var router = express.Router();
var DATE_FORMAT = 'DD/MM/yyyy, h:mm:ss A';

/* GET home page. */
router.get('/tilt', function(req, res, next) {
  var results = [];
  var path = "./data/";
  var demo = "";
  var celsius = true;

  var files = getDirectoryFiles(path);

  if (req.query.demo || files.length === 0) {
    path += "demo/";
    files = getDirectoryFiles(path);
    demo = " :: No Data Received :: Demo Mode";
  }

  if (req.query.celsius) {
    celsius = (req.query.celsius === 'true');
  }

  for (let i = 0; i < files.length; i++) {
    const data = fs.readFileSync(path+files[i], {encoding:'utf8', flag:'r'});
    results.push(enhanceData(JSON.parse(data), celsius));
  }

  res.render('index', {'results': results, 'generated': moment().format(DATE_FORMAT), 'demo': demo});
});

function getDirectoryFiles(path) {
  const dirents = fs.readdirSync(path, { withFileTypes: true });
  return dirents
      .filter(dirent => dirent.isFile())
      .map(dirent => dirent.name);
}

function enhanceData(payload, celsius) {
  var min = 1.000;
  var max = 1.0925;
  
  var formattedGravity = numeral(payload.SG).format("0.000");
  var temperature = celsius? numeral((5/9) * (payload.Temp - 32)).format("0.0") : payload.Temp;


  var gravityPercent = ((formattedGravity - min) * 100) / (max - min);

  payload.formattedGravity = formattedGravity;
  payload.gravityPercent = gravityPercent;
  payload.temperature = temperature;
  payload.temperatureType = celsius ? "C" : "F";
  

  payload.duration = moment.duration(moment(payload.timestamp, DATE_FORMAT).diff(moment())).humanize()
  
  return payload;
}

router.post('/', function(req, res) {
  req.body.timestamp = moment().format(DATE_FORMAT)
  var json = JSON.stringify(req.body);
  
  fs.writeFile("./data/result-"+req.body.Color+".json", json, function(err) {
    if (err) {
      console.log(err);
    }
  });
  res.status(204).send();
});

module.exports = router;